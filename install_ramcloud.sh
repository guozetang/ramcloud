#!/bin/bash
#This is a script to install ramcloud.
export PATH=/bin:/sbin:/usr/bin:/usr/sbin:/usr/local/bin:/usr/local/sbin:~/bin
clear
CURRENT_DIR=$(pwd)
PRPCESSOR_NUM=$(getconf _NPROCESSORS_ONLN)

echo "[INFO] Begin to install RAMCloud...."
cd $CURRENT_DIR
make clean
make -j $PRPCESSOR_NUM DEBUG=no install INSTALL_DIR=/usr/local
sudo echo "/usr/local/lib/ramcloud/" > /etc/ld.so.conf.d/ramcloud.conf
sudo ldconfig
sudo cp src/*.h /usr/local/include/ramcloud/
echo "[OK] Install RAMCloud Done!"
exit 0