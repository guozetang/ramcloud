#!/bin/bash
# Author: Guoze Tang
# Blog: guozet.me
# Note: Install RAMCloud on Ubuntu 14.04 or 16.04
# Version: V1.1

export PATH=/bin:/sbin:/usr/bin:/usr/sbin:/usr/local/bin:/usr/local/sbin:~/bin
clear
CURRENT_DIR=$(pwd)
PRPCESSOR_NUM=$(getconf _NPROCESSORS_ONLN)

if [ `whoami` != "root" ];then
	echo "You must have root privileges to run this script."
    exit 1
fi

echo "[INFO] Installing Dependencies"
echo "[INFO] Clean the workbench for the packages compile."
cd $CURRENT_DIR/packages
for pkg in `ls`; do
    if [ -d $pkg ]; then
	rm -rf $pkg
    echo "[OK] Remove the $CURRENT_DIR/packages/$pkg directory."
    fi
done
cd $CURRENT_DIR
source dependencies.sh
source /etc/profile

echo "[INFO] Begin to install RAMCloud...."
cd $CURRENT_DIR
make clean
make -j $PRPCESSOR_NUM DEBUG=no install INSTALL_DIR=/usr/local
sudo echo "/usr/local/lib/ramcloud/" > /etc/ld.so.conf.d/ramcloud.conf
sudo ldconfig
sudo cp src/*.h /usr/local/include/ramcloud/
echo "[OK] Install RAMCloud Done!"
exit 0